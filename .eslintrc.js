module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/strongly-recommended',
    "@nuxtjs/eslint-config-typescript",
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: [
    'vue'
  ],
  // add your custom rules here
  rules: {
    'func-call-spacing': "off",
    'semi': 'off',
    'vue/no-v-html': 'off',
    'dot-notation': 'off',
    'no-trailing-spaces': 'off'
  }
}

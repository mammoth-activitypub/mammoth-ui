'use strict';
/// <reference types="jest" />
/// <reference types="axios" />

import { shallowMount, createLocalVue } from '@vue/test-utils';
import ElementUI from 'element-ui';
import Index from './index.vue';

const localVue = createLocalVue();
localVue.use(ElementUI);

function dataFn (overrides?: any) {
  const data = {
    userId: '',
    password: '',
    email: '',
    error: false,
    errorMessage: '',
    login: true
  };
  if (overrides) {
    for (const [key, value] of Object.entries(overrides)) {
      // @ts-ignore
      data[key] = value;
    }
  }
  return () => data;
}

const $axios = {
  $get: jest.fn()
}

beforeEach(() => {
  $axios.$get.mockReset();
  $axios.$get.mockResolvedValueOnce(false);
});

describe('login/signup choice', () => {
  test('show the login screen when the login selection is made', () => {
    $axios.$get.mockResolvedValueOnce(false);
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn(),
      mocks: {
        $axios
      }
    });
    expect(wrapper.findComponent({ ref: 'login' }).exists()).toBeTruthy();
    expect(wrapper.findComponent({ ref: 'signup' }).exists()).toBeFalsy();
  });

  test('show the signup screen when the signup selection is made', () => {
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn({ login: false }),
      mocks: {
        $axios
      }
    });
    expect(wrapper.findComponent({ ref: 'login' }).exists()).toBeFalsy();
    expect(wrapper.findComponent({ ref: 'signup' }).exists()).toBeTruthy();
  });

  test.skip('change the view when clicking the login/signup link to signup', async () => {
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn(),
      mocks: {
        $axios
      }
    });
    // @ts-ignore
    wrapper.$axios = {
      $get: jest.fn()
    }
    const signupLink = wrapper.findComponent({ ref: 'signupLink' });
    await signupLink.trigger('click');
    expect(wrapper.vm.$data.login).toBeFalsy();
  });

  test.skip('change the view when clicking the login/signup link to signin', async () => {
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn({ login: false }),
      mocks: {
        $axios
      }
    });
    await wrapper.findComponent({ ref: 'signinLink' }).trigger('click');
    expect(wrapper.vm.$data.login).toBeTruthy();
  });

  test.skip('submit data to login page when logging in', async () => {
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn({ userId: 'bob', password: 'password', error: true, errorMessage: 'dammit' }),
      mocks: {
        $axios
      }
    });
    const setActorCookie = jest.fn();
    const post = jest.fn();
    post.mockResolvedValue({ userId: 'actor' });
    const push = jest.fn();
    // @ts-ignore
    wrapper.vm.setActorCookie = setActorCookie;
    // @ts-ignore
    wrapper.vm.$axios = { post };
    // @ts-ignore
    wrapper.vm.$router = { push };
    // @ts-ignore
    await wrapper.vm.submit();

    expect(post.mock.calls.length).toBe(1);
    expect(post.mock.calls[0][1]).toEqual({ userId: 'bob', password: 'password' });
    expect(setActorCookie.mock.calls.length).toBe(1);
    expect(push.mock.calls.length).toBe(1);
    expect(push.mock.calls[0][0]).toBe('timeline');
    expect(wrapper.vm.$data.error).toBeFalsy();
    expect(wrapper.vm.$data.errorMessage).toBe('');
  });

  test('sets appropriate message when login fails bc/of authentication failure', async () => {
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn({ userId: 'bob', password: 'password' }),
      mocks: {
        $axios
      }
    });
    // @ts-ignore
    wrapper.$axios = {
      $get: jest.fn()
    }
    const post = jest.fn();
    post.mockRejectedValue({ statusCode: 401 });
    // @ts-ignore
    wrapper.vm.$axios = { post };
    // @ts-ignore
    await wrapper.vm.submit();

    expect(wrapper.vm.$data.error).toBeTruthy();
    expect(wrapper.vm.$data.errorMessage).toBe('Username or password not valid.');
  });

  test.skip('sets appropriate message when login fails bc of unknown error', async () => {
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn({ userId: 'bob', password: 'password' }),
      mocks: {
        $axios
      }
    });
    const post = jest.fn();
    post.mockRejectedValue({ statusCode: 500 });
    // @ts-ignore
    wrapper.vm.$axios = { post };
    // @ts-ignore
    await wrapper.vm.submit();

    expect(wrapper.vm.$data.error).toBeTruthy();
    expect(wrapper.vm.$data.errorMessage).toBe('An unknown server error occurred. Please try again later.');
  });

  test('setActorCookie sets cookie correctly', () => {
    const wrapper = shallowMount(Index, { localVue, data: dataFn() });
    const setCookieMock = jest.fn();
    // @ts-ignore
    wrapper.vm.$cookies = {
      set: setCookieMock
    };
    const actor = { userId: 'bob' };
    // @ts-ignore
    wrapper.vm.setActorCookie(actor);
    const now = Date.now();
    // @ts-ignore
    Date.now = jest.spyOn(Date, 'now').mockImplementation(() => now);

    expect(setCookieMock.mock.calls.length).toBe(1);
    expect(setCookieMock.mock.calls[0][0]).toBe('actor');
    expect(setCookieMock.mock.calls[0][1]).toBe('eyJ1c2VySWQiOiJib2IifQ==');
    expect(setCookieMock.mock.calls[0][2].sameSite).toBeTruthy();
    expect(setCookieMock.mock.calls[0][2].expires).toEqual(new Date(now + 86400000));
  });
});

describe('signup', () => {
  const userData = { userId: 'bob', password: 'password', email: 'bob@here.org' };
  test('signup works when everything is ok', async () => {
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn(userData),
      mocks: {
        $axios
      }
    });
    const post = jest.fn();
    post.mockResolvedValue({ });
    const submit = jest.fn();
    // @ts-ignore
    wrapper.vm.submit = submit;
    // @ts-ignore
    wrapper.vm.$axios = { post };
    // @ts-ignore
    await wrapper.vm.signup();
    expect(post.mock.calls.length).toBe(1);
    expect(post.mock.calls[0][0]).toBe('user');
    expect(post.mock.calls[0][1]).toEqual(userData);
    expect(submit.mock.calls.length).toBe(1);
    expect(wrapper.vm.$data.error).toBeFalsy();
  });

  test('signup fails well when user already exists', async () => {
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn(userData),
      mocks: {
        $axios
      }
    });
    const post = jest.fn();
    post.mockRejectedValue({ statusCode: 409, message: 'you are bad' });
    const submit = jest.fn();
    // @ts-ignore
    wrapper.vm.submit = submit;
    // @ts-ignore
    wrapper.vm.$axios = { post };
    // @ts-ignore
    await wrapper.vm.signup();
    expect(wrapper.vm.$data.error).toBeTruthy();
    expect(wrapper.vm.$data.errorMessage).toBe('An account with that user id already exists.');
    expect(submit.mock.calls.length).toBe(0);
  });

  test('signup fails well when unknown error occurs', async () => {
    const wrapper = shallowMount(Index, {
      localVue,
      data: dataFn(userData),
      mocks: {
        $axios
      }
    });
    const post = jest.fn();
    post.mockRejectedValue({ statusCode: 500, message: 'stuff happened' });
    const submit = jest.fn();
    // @ts-ignore
    wrapper.vm.submit = submit;
    // @ts-ignore
    wrapper.vm.$axios = { post };
    // @ts-ignore
    await wrapper.vm.signup();
    expect(wrapper.vm.$data.error).toBeTruthy();
    expect(wrapper.vm.$data.errorMessage).toBe('An unknown error occurred.');
    expect(submit.mock.calls.length).toBe(0);
  });
});

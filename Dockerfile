FROM node:latest as build

COPY . /node/

WORKDIR /node

RUN yarn && \
    npm run export

FROM alpine:3.12
LABEL maintainer="Brad Koehn" \
      version="1.0.0"

# Install packages
RUN apk update && \
    apk add --no-cache \
    lighttpd && \
    rm -rf /var/cache/apk/*

# Expose http(s) ports
EXPOSE 80

# Make configuration path and webroot a volume
VOLUME /etc/lighttpd/
VOLUME /var/www/

ENTRYPOINT ["/usr/sbin/lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]

COPY --from=build /node/dist /var/www/localhost/htdocs

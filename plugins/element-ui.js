import Vue from 'vue'
import {
  Dialog,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  Input,
  Select,
  Option,
  OptionGroup,
  Button,
  ButtonGroup,
  Tooltip,
  Form,
  FormItem,
  Icon,
  Card,
  Carousel,
  CarouselItem,
  Container,
  Header,
  Aside,
  Main,
  Footer,
  Link,
  Image,
  Message,
  Avatar,
  Upload,
  Loading
} from 'element-ui';

import locale from 'element-ui/lib/locale/lang/en'

Vue.use(Avatar, { locale })
Vue.use(Dialog, { locale })
Vue.use(Dropdown, { locale })
Vue.use(DropdownMenu, { locale })
Vue.use(DropdownItem, { locale })
Vue.use(Menu, { locale })
Vue.use(Submenu, { locale })
Vue.use(MenuItem, { locale })
Vue.use(MenuItemGroup, { locale })
Vue.use(Input, { locale })
Vue.use(Select, { locale })
Vue.use(Option, { locale })
Vue.use(OptionGroup, { locale })
Vue.use(Button, { locale })
Vue.use(ButtonGroup, { locale })
Vue.use(Tooltip, { locale })
Vue.use(Form, { locale })
Vue.use(FormItem, { locale })
Vue.use(Icon, { locale })
Vue.use(Card, { locale })
Vue.use(Carousel, { locale })
Vue.use(CarouselItem, { locale })
Vue.use(Container, { locale })
Vue.use(Header, { locale })
Vue.use(Aside, { locale })
Vue.use(Main, { locale })
Vue.use(Footer, { locale })
Vue.use(Link, { locale })
Vue.use(Image, { locale })
Vue.use(Upload, { locale })
Vue.use(Loading, { locale })

Vue.prototype.$message = Message;

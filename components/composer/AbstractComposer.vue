<script lang="ts">
import Component from 'nuxt-class-component';
import { Prop, Watch } from 'vue-property-decorator';
import type { ASObject, Collection, Link, ObjectRef, ObjectRefs, OrderedCollection } from 'mammoth-activitystreams';
import { ObjectType, resolveLinkRef, resolveObjectRef } from 'mammoth-activitystreams';
import type { Actor } from 'mammoth-activitypub';
import AuthenticatedComponent from '../AuthenticatedComponent.vue';

const showdown = require('showdown');
showdown.setOption('requireSpaceBeforeHeadingText', true);
const converter = new showdown.Converter();
const _ = require('lodash');

export const hashTagPattern = /(?:^|\B)#(?![\p{Nd}\p{Pc}]+\b)([\p{L}\p{Nl}\p{Nd}\p{Pc}]{1,30})(?:\b|\r)?/gu

@Component({
  abstract: true
})
export default abstract class Composer extends AuthenticatedComponent {
  @Prop(Object) readonly original: ASObject
  audience: string = 'Public'
  content: string = ''
  attachments: ObjectRef<ASObject>[] = []
  creatingPost = false
  debouncedScanFNLA = _.debounce(this.scanForNewLinkAttachments);

  hashTags (): string[] {
    return this.content.match(hashTagPattern);
  }

  async created (): Promise<void> {
    await this.getActor(); // FIXME should all super.created()
    this.getBus().$on('uploadedImage-set-rotation', this.handleRotationUpdateEvent);
    this.getBus().$on('uploadedImage-set-image', this.handleImageUpdateEvent);
  }

  beforeDestroy (): void {
    this.getBus().$off('uploadedImage-set-rotation', this.handleRotationUpdateEvent);
    this.getBus().$off('uploadedImage-set-image', this.handleImageUpdateEvent);
  }

  handleRotationUpdateEvent (evt) {
    this.$set(evt.uploadedImage, 'rotation', evt.rotation);
  }

  handleImageUpdateEvent (evt) {
    this.$set(evt.uploadedImage, 'img', evt.image);
  }

  async uploadImage (img: HTMLImageElement, rotation: number, width: number, uploadedImage: ASObject): Promise<any> {
    try {
      const { canvasWidth, canvasHeight, blob } = await this.createScaledImage(width, rotation, img, uploadedImage);
      (blob as any).lastModifiedDate = new Date();
      (blob as any).name = uploadedImage.href;
      const data = new FormData();
      data.append('file', blob, uploadedImage.href);
      const location: Link = await this.$axios.$post('/image', data, {
        headers: {
          'content-type': `multipart/form-data; boundary=${(data as any)._boundary}`
        }
      });

      location.width = canvasWidth;
      location.height = canvasHeight;
      return location;
    } catch (e) {
      this.$message.error('Unable to upload image.');
      throw e;
    }
  }

  getMimeTypeFromExtension (filename: string): string {
    if (!filename) {
      return undefined;
    }

    const extension = filename.split('.').pop().toLowerCase();
    switch (extension) {
      case 'jpg':
      case 'jpeg':
        return 'image/jpeg';

      case 'png':
        return 'image/png';

      case 'gif':
        return 'image/gif';
    }
    return undefined;
  }

  /**
   * So it turns out that pasted images will always be of type `image/png` and named
   * `image.png`, but if you inspect the clipboard text contents you can get the original
   * filename and derive the mediaType from that. But getting the filename can only
   * happen asynchronously, while getting the file can only happen in an event handler,
   * so we have this hairball of code. Thanks, past me, for documenting this.
   *
   * You're welcome, future me.
   */
  handlePaste (evt) {
    const result: any[] = evt.clipboardData.items;
    let lastTextDataTransferItem: DataTransferItem = null;
    let lastAttachment: ASObject = null;
    for (const item: DataTransferItem of result) {
      if (item.type === 'text/plain') {
        lastTextDataTransferItem = item;
        continue;
      }
      if (!item.type.includes('image')) {
        continue;
      }
      const file = item.getAsFile();
      const attachment = {
        type: 'UploadedImage',
        mediaType: 'image/jpeg',
        href: file.name,
        file,
        img: null,
        rotation: 0
      };
      lastAttachment = attachment as ASObject;
      this.attachments.push(attachment as ObjectRef<ASObject>);
    }
    if (lastTextDataTransferItem && lastAttachment) {
      lastTextDataTransferItem.getAsString((filename) => {
        const mimeType = this.getMimeTypeFromExtension(filename);
        if (mimeType) {
          lastAttachment.mediaType = mimeType;
        }
      });
    }
  }

  async createScaledImage (width: number, rotation: number, img: HTMLImageElement, uploadedImage: ASObject) {
    const canvas = document.createElement('canvas');
    canvas.width = width;
    if (rotation === 0 || rotation === Math.PI) {
      canvas.height = (img.height / img.width) * width;
    } else {
      canvas.height = (img.width / img.height) * width;
    }
    const ctx = canvas.getContext('2d');
    const imageWidth = img.width;
    const imageHeight = img.height;
    const canvasWidth = canvas.width;
    const canvasHeight = canvas.height;

    let scale;
    if (rotation === 0 || rotation === Math.PI) {
      scale = canvasWidth / imageWidth;
    } else {
      scale = canvasWidth / imageHeight;
    }
    ctx.setTransform(scale, 0, 0, scale, canvasWidth / 2, canvasHeight / 2);
    ctx.rotate(rotation);
    ctx.drawImage(img, -imageWidth / 2, -imageHeight / 2);
    const blob = await new Promise<Blob>((resolve) => {
      canvas.toBlob(resolve, uploadedImage.mediaType, 0.60);
    });
    return { canvasWidth, canvasHeight, blob };
  }

  async uploadNewMediaAttachments () {
    const objectPromises = [];
    for (const o: ASObject of this.attachments) {
      if (o.type === 'UploadedImage') {
        const rotation = o.rotation;
        const promises = [];
        const img = o.img;
        promises.push(this.uploadImage(img, rotation, Math.min(img.width, 600), o));
        if (img.width > 1024) {
          promises.push(this.uploadImage(img, rotation, 1024, o));
        }
        o.type = ObjectType.Image;
        this.$delete(o, 'file');
        this.$delete(o, 'img');
        this.$delete(o, 'rotation');
        this.$delete(o, 'href');
        objectPromises.push(Promise.all(promises).then((results: any) => {
          o.url = results;
        }));
      }
    }
    await Promise.all(objectPromises);
  }

  newCollection (): Collection<any> {
    return {
      type: ObjectType.Collection,
      totalItems: 0,
      items: []
    }
  }

  newOrderedCollection (): OrderedCollection<any> {
    return {
      type: ObjectType.OrderedCollection,
      totalItems: 0,
      orderedItems: []
    }
  }

  createToAddressees (actor: Actor): ObjectRefs<Actor | Collection<ASObject>> {
    switch (this.audience) {
      case 'Public':
        return [
          'https://www.w3.org/ns/activitystreams#Public',
          resolveLinkRef(actor.followers as any)
        ] as ObjectRefs<Actor | Collection<ASObject>>

      case 'Followers':
        return [
          resolveLinkRef(actor.followers as any)
        ] as ObjectRefs<Actor | Collection<ASObject>>

      case 'Friends':
        return [
          resolveLinkRef(actor.followers as any),
          resolveLinkRef(actor.friends)
        ] as ObjectRefs<Actor | Collection<ASObject>>
    }
  }

  abstract createPostObject(): ASObject;

  afterPostCompleted (): void {
  }

  createHtmlContent (): string {
    return converter.makeHtml(this.content.trim());
  }

  createMarkdownContent (content: string): string {
    return converter.makeMarkdown(content.trim());
  }

  async createPost () {
    try {
      this.creatingPost = true;
      await this.uploadNewMediaAttachments();
      const post: ASObject = this.createPostObject();
      if (post) {
        const tags = this.hashTags();
        if (tags) {
          post.tag = tags;
        }
        const outbox = resolveLinkRef(this.me.outbox as any);
        const response = await this.$axios.post(outbox, post);
        post.id = response.headers.location;
        post.attributedTo = this.me;
        post.likes = this.newCollection();
        post.shares = this.newCollection();
        post.replies = this.newOrderedCollection();
        post.published = new Date().toISOString();
        if (this.original) {
          this.getBus().$emit('post-updated', post);
        } else {
          this.getBus().$emit('post-created', post);
        }
        this.content = '';
        this.audience = 'Public';
        this.afterPostCompleted();
        this.$message.info('post saved');
      }
    } catch (e) {
      if (e.response && e.response.status === 401) {
        await this.$router.push('/')
      } else {
        this.$message.error('unable to create post');
      }
    }
    this.creatingPost = false;
  }

  isValidHttpUrl (s: string): boolean {
    if (!s.startsWith('http')) {
      return false;
    }

    let url;

    try {
      url = new URL(s);
    } catch (_) {
      return false;
    }

    return url.protocol === 'http:' || url.protocol === 'https:';
  }

  scanForLinks (content: string): string[] {
    const tokens = content.split(/\s/);
    const links: string[] = [];
    tokens.forEach((token: string) => {
      if (this.isValidHttpUrl(token)) {
        links.push(token);
      }
    });
    return links;
  }

  async newLink (url: string): Promise<ASObject> {
    return await this.$axios.$get('/link-preview',
      { params: { url } });
  }

  @Watch('content')
  scanPeriodicallyForNewLinkAttachments (newContent: string, oldContent: string) {
    this.debouncedScanFNLA(newContent, oldContent);
  }

  scanForNewLinkAttachments (newContent: string, oldContent: string) {
    const newContentLinks = this.scanForLinks(newContent);
    const oldContentLinks = this.scanForLinks(oldContent);
    let netNewLinks;
    if (oldContent) {
      netNewLinks = _.difference(newContentLinks, oldContentLinks);
    } else {
      netNewLinks = newContentLinks;
    }
    // need to upgrade existing links that are now longer
    netNewLinks = netNewLinks.filter((link: string) =>
      !this.attachments.find((attachment: any): boolean =>
        link.startsWith(resolveObjectRef(attachment))));

    netNewLinks.forEach(async (link: string) => {
      try {
        this.attachments.push(await this.newLink(link));
        // eslint-disable-next-line no-empty
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e);
      }
    });
  }
}
</script>

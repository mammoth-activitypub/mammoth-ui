'use strict';
/// <reference types="jest" />
/// <reference types="axios" />

import { createLocalVue, mount, shallowMount } from '@vue/test-utils';
import type { Like } from 'mammoth-activitystreams';
import ElementUI, { Button } from 'element-ui';
import flushPromises from 'flush-promises';
import ReactionButtons from './ReactionButtons.vue';

let propsData: any = null;
const localVue = createLocalVue();
localVue.use(ElementUI);

const createProps = () => {
  return {
    reactable: {
      id: 'reactableId',
      postId: 'postId',
      objectId: 'objectId',
      likes: {
        totalItems: 0,
        items: []
      },
      shares: {
        totalItems: 0,
        items: []
      }
    },
    __testMe: {
      id: 'actorId',
      preferredUsername: 'Bob'
    }
  };
};

beforeEach(() => {
  propsData = createProps();
});

describe('ReactionButtons', () => {
  test.skip('highlights like button when liked', async () => {
    propsData.reactable.likes.items.push('Bob');
    const wrapper = shallowMount(ReactionButtons, { propsData, localVue });
    await flushPromises();
    const likeButton = wrapper.findComponent({ ref: 'likeButton' });
    expect(likeButton.exists()).toBe(true);
    expect(likeButton.text()).toBe('unlike');
  });

  it.skip('doesn’t highlight like button when not liked', () => {
    const wrapper = shallowMount(ReactionButtons, { propsData, localVue });
    const likeButton = wrapper.findComponent({ ref: 'likeButton' });
    expect(likeButton.exists()).toBe(true);
    expect(likeButton.text()).toBe('like');
  });
});

describe('reaction methods', () => {
  it('correctly determines if user doesn’t like this reactable', () => {
    const wrapper = shallowMount(ReactionButtons, { propsData, localVue });
    // @ts-ignore
    expect(wrapper.vm.isLiked).toBeFalsy();
  });

  it.skip('correctly determines if user likes this reactable', () => {
    propsData.reactable.likes = ['Bob'];
    const wrapper = shallowMount(ReactionButtons, { propsData, localVue });
    // @ts-ignore
    expect(wrapper.vm.isLiked).toBeTruthy();
  });

  it('correctly determines if user dislikes this reactable', () => {
    const wrapper = shallowMount(ReactionButtons, { propsData });
    // @ts-ignore
    expect(wrapper.vm.isDisliked).toBeFalsy();
  });

  it.skip('correctly determines if user doesn\'t dislike this reactable', () => {
    propsData.reactable.dislikes = ['Bob'];
    const wrapper = shallowMount(ReactionButtons, { propsData, localVue });
    // @ts-ignore
    expect(wrapper.vm.isDisliked).toBeTruthy();
  });

  describe('reaction button click tests', () => {
    let wrapper: any = null;
    let submitReactionMock: any = null;
    let undoReactionMock: any = null;

    beforeEach(() => {
      wrapper = shallowMount(ReactionButtons, { propsData, localVue });
      wrapper.vm.submitReaction = submitReactionMock = jest.fn();
      wrapper.vm.undoReaction = undoReactionMock = jest.fn();
    });

    it.skip('correctly submits a like request when user clicks the button', async () => {
      wrapper.findComponent({ ref: 'likeButton' }).trigger('click');
      await wrapper.vm.$nextTick();
      await wrapper.vm.$nextTick();

      expect(submitReactionMock.mock.calls.length).toBe(1);
      expect(submitReactionMock.mock.calls[0][0]).toBe('Like');
      expect(undoReactionMock.mock.calls.length).toBe(0);
    });

    it.skip('correctly submits a dislike request when user clicks the dislike button', async () => {
      wrapper.findComponent({ ref: 'dislikeButton' }).trigger('click');
      await wrapper.vm.$nextTick();
      await wrapper.vm.$nextTick();

      expect(submitReactionMock.mock.calls.length).toBe(1);
      expect(submitReactionMock.mock.calls[0][0]).toBe('Dislike');
      expect(undoReactionMock.mock.calls.length).toBe(0);
    });

    it.skip('undoes a previous like when like button pushed', async () => {
      propsData = createProps();
      propsData.reactable.likes = ['Bob'];
      propsData.reactable.usersReaction = 'http://userreaction';
      wrapper.setProps(propsData);
      wrapper.findComponent({ ref: 'likeButton' }).trigger('click');
      await wrapper.vm.$nextTick();
      await wrapper.vm.$nextTick();

      expect(submitReactionMock.mock.calls.length).toBe(0);
      expect(undoReactionMock.mock.calls.length).toBe(1);
    });

    it.skip('undoes a previous dislike', async () => {
      propsData = createProps();
      propsData.reactable.dislikes = ['Bob'];
      propsData.reactable.usersReaction = 'http://userreaction';
      wrapper.setProps(propsData);
      wrapper.findComponent({ ref: 'dislikeButton' }).trigger('click');
      await wrapper.vm.$nextTick();
      await wrapper.vm.$nextTick();

      expect(submitReactionMock.mock.calls.length).toBe(0);
      expect(undoReactionMock.mock.calls.length).toBe(1);
    });

    it.skip('undoes a previous like when dislike clicked', async () => {
      propsData = createProps();
      propsData.reactable.likes = ['Bob'];
      propsData.reactable.usersReaction = 'http://userreaction';
      wrapper.setProps(propsData);
      wrapper.findComponent({ ref: 'dislikeButton' }).trigger('click');
      await wrapper.vm.$nextTick();
      await wrapper.vm.$nextTick();

      expect(submitReactionMock.mock.calls.length).toBe(1);
      expect(submitReactionMock.mock.calls[0][0]).toBe('Dislike');
      expect(undoReactionMock.mock.calls.length).toBe(1);
    });

    it.skip('undoes a previous dislike when like clicked', async () => {
      propsData = createProps();
      propsData.reactable.dislikes = ['Bob'];
      propsData.reactable.usersReaction = 'http://userreaction';
      wrapper.setProps(propsData);
      wrapper.findComponent({ ref: 'likeButton' }).trigger('click');
      await wrapper.vm.$nextTick();
      await wrapper.vm.$nextTick();

      expect(submitReactionMock.mock.calls.length).toBe(1);
      expect(submitReactionMock.mock.calls[0][0]).toBe('Like');
      expect(undoReactionMock.mock.calls.length).toBe(1);
    });
  });

  describe('tests that call backend services', () => {
    let wrapper: any = null;
    let postMock: any = null;

    beforeEach(() => {
      wrapper = shallowMount(ReactionButtons, { propsData, localVue });
      postMock = jest.fn();
      const response = {
        headers: {
          location: 'http://userreaction'
        }
      };
      postMock.mockResolvedValue(response);
      // @ts-ignore
      wrapper.vm.$axios = ({
        post: postMock
      });
    });

    test.skip('undoReaction does what it should', async () => {
      propsData.reactable.usersReaction = 'http://userreaction';
      propsData.reactable.likes = ['Bob'];
      // @ts-ignore
      await wrapper.vm.undoReaction(propsData.reactable, propsData.actor);
      await wrapper.vm.$nextTick();
      await wrapper.vm.$nextTick();

      expect(postMock.mock.calls.length).toBe(1);
      expect(postMock.mock.calls[0][1].type).toBe('Undo');
      expect(wrapper.vm.isLiked).toBeFalsy();
      expect(wrapper.vm.$props.reactable.usersReaction).toBeFalsy();
    });

    test.skip('submitReaction does what it should', async () => {
      const likes: Like[] = [];
      await wrapper.vm.submitReaction('Like', likes);
      await wrapper.vm.$nextTick();
      await wrapper.vm.$nextTick();

      expect(postMock.mock.calls.length).toBe(1);
      expect(postMock.mock.calls[0][1].type).toBe('Like');
      expect(postMock.mock.calls[0][1].object).toBe('objectId');
      expect(wrapper.props().reactable.usersReaction).toBe('http://userreaction');
    });

    test.skip('it removes from collection', () => {
      const collection = ['foo', 'bar', 'baz'];

      // @ts-ignore
      expect(wrapper.vm.removeFromCollection(collection, 'bar')).toEqual(['foo', 'baz']);
    });
  });
});

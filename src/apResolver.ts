'use strict';

import type { ASObject, Link, ObjectRef, ObjectRefs } from 'mammoth-activitystreams';
import { objectRefsType, objectRefType } from 'mammoth-activitystreams';
import { FetchInstructions } from 'mammoth-api';

const Axios = require('axios');

interface CacheEntry {
  expires: number;
  promise: Promise<ASObject>;
}

const resolvedObjectCache: Record<string, CacheEntry> = {};
setInterval(() => {
  for (const [key, value] of Object.entries(resolvedObjectCache)) {
    if (value.expires < Date.now()) {
      delete resolvedObjectCache[key];
    }
  }
}, 60000);

export function fetchObject<T extends ASObject> (id?: string, fetch?: FetchInstructions): Promise<T> {
  if (!id) {
    // @ts-ignore
    return Promise.resolve(null);
  }

  const headers = {
    Accept: 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
  }
  if (resolvedObjectCache[id]) {
    const cacheEntry: CacheEntry = resolvedObjectCache[id];
    if (cacheEntry.expires < Date.now()) {
      return cacheEntry.promise as Promise<T>;
    }
  }

  if (id.startsWith(window.location.hostname)) {
    // avoid fetch API for IDs we can resolve ourselves
    const promise = Axios.get(id).then((result: any) => result.data);
    resolvedObjectCache[id] = {
      expires: Date.now() + 60,
      promise
    };
    return promise;
  }

  const params = {
    id,
    fetch // should this be encoded somehow?
  };

  // todo need to get $axios in here somehow
  const promise = Axios.get('/api/fetch', { params, headers }).then((result: any) => result.data);
  // .catch((e: Error) => console.info(`fetch of ${id} failed: ${e.message}`));
  resolvedObjectCache[id] = {
    expires: Date.now() + 60,
    promise
  };
  return promise;
}

export function resolveObject<T extends ASObject> (objectRef: ObjectRef<T>, fetch?: FetchInstructions): Promise<T> {
  switch (objectRefType(objectRef)) {
    case 'string':
      return fetchObject(objectRef as string, fetch);
    case 'Link':
      return fetchObject((objectRef as Link).href, fetch);
    case 'ASObject':
      return Promise.resolve(objectRef as T);
    default:
      return Promise.resolve(null as unknown as T);
  }
}

export function resolveObjects<T extends ASObject> (objectRefs: ObjectRefs<T>, fetch?: FetchInstructions): Promise<T> {
  if (objectRefsType(objectRefs) === 'array') {
    const promises: Promise<ASObject>[] = [];
    const refs = objectRefs as ObjectRef<T>[];
    refs.forEach((ref: ObjectRef<T>) => {
      promises.push(resolveObject(ref, fetch));
    });
    // @ts-ignore
    return Promise.all(promises);
  }
  return resolveObject(objectRefs as ObjectRef<T>, fetch);
}

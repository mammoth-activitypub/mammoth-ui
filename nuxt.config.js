require('dotenv').config();
const dev = process.env.NODE_ENV !== 'production';
export default {
  mode: 'spa',
  target: 'static',
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },

  /*
  ** Headers of the page
  */
  head: {
    title: 'Mammoth',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: false,
  /*
  ** Global CSS
  */
  css: [
    'element-ui/lib/theme-chalk/index.css'
  ],
  render: {
    csp: {
      reportOnly: false,
      hashAlgorithm: 'sha256',
      policies: {
        'default-src': ["'self'"],
        'img-src': dev ? undefined : ['https:', 'data:'],
        'media-src': dev ? undefined : ['https:'],
        'connect-src': dev ? undefined : ['https:'],
        'style-src': ["'self'"],
        'script-src': ["'self'"],
        'form-action': ["'self'"],
        'frame-ancestors': ["'none'"],
        'object-src': ["'none'"],
        'base-uri': ["'self'"]
      },
      addMeta: false
    }
  },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/element-ui',
    '@/plugins/axios'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxt/typescript-build',
    '@nuxtjs/eslint-module',
    '@nuxtjs/moment'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    ['cookie-universal-nuxt', { parseJSON: false }],
    ['@dansmaculotte/nuxt-security', {
      hsts: {
        maxAge: 15552000,
        includeSubDomains: true,
        preload: true
      },
      referrer: 'same-origin',
      additionalHeaders: true
    }]
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    proxy: true,
    prefix: '/api/'
  },
  /*
  ** Build configuration
  */
  build: {
    // analyze: {
    //   analyzerMode: 'static'
    // },
    // fix for https://github.com/nuxt/nuxt.js/issues/3828
    // filenames: {
    //   app: ({ isDev }) => isDev ? '[name].[hash].js' : '[chunkhash].js',
    //   chunk: ({ isDev }) => isDev ? '[name].[hash].js' : '[chunkhash].js'
    // },
    babel: {
      plugins: ['transform-class-properties']
    },
    transpile: [/^element-ui/, /^vue-moment/],
    extractCSS: true,
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(ts|js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (ctx.isClient) {
        config.optimization.splitChunks.maxSize = 200000;
        config.devtool = 'source-map';
      }
    }
  }
}
